function saveOptions() {
	var url=document.getElementById("jira").value;
  var error=document.getElementById("error");

  if (url === "") {
    error.textContent="Please fill URL";
  } else {
    error.innerText="";

		browser.storage.local.set({
			jira: url
		});
	}
}

function restoreOptions() {
	browser.storage.local.get({
		jira: "http://example.com"
	}, function (items) {
		document.getElementById("jira").value=items.jira;
		document.getElementById("jira").select();
	});
}
document.addEventListener("DOMContentLoaded", restoreOptions);
document.getElementById("save").addEventListener("click", saveOptions);

var enter=13;

function inputURLListener(e) {
	if (e.keyCode === enter) {
		saveOptions();
	}
}

function listenInputURL(inputURL) {
	if (inputURL.addEventListener) {
		inputURL.addEventListener("keydown", inputURLListener, false);
	} else if (inputURL.attachEvent) {
		inputURL.attachEvent("keydown", inputURLListener);
	}
}

function listenURL() {
	listenInputURL(document.getElementById("jira"));
}

if (window.addEventListener) {
	window.addEventListener("load", listenURL, false);
} else if (window.attachEvent) {
	window.attachEvent("onload", listenURL);
} else {
	document.addEventListener("load", listenURL, false);
}
