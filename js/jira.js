var key;
var list=[];
var url;
var isError=0;

function setErrorText(errorText) {
	var divError=document.getElementById("error");
	divError.innerText=errorText;
};

function returnError(errorText) {
	setErrorText(errorText);
	throw "";
};

function removeSpaces(string) {
	while (string.charAt(string.length - 1) === " ") {
		string=string.slice(0, string.length - 1);
	}

	if (string.charAt(0) === " ") {
		var temp=string.split(" ");
		string=temp[temp.length - 1];
	}

	return string;
};

function removeSkypeFormatting(string) {
	if (string.charAt(0) === "[") {
		var temp=string.split(" ");
		string=temp[temp.length - 1];
	}

	return string;
};

function openWindow() {
	window.open(url + key);
	window.close();
};

function onGot(item) {
	url=item.jira + "/browse/";

	key=document.getElementById("key").value;
	key=removeSpaces(key);
	key=removeSkypeFormatting(key);

	if (key === "") {
		isError=1;
		returnError("Please insert key");
	} else if (url === undefined) {
		isError=1;
		returnError("Please define URL in Options");
	} else {
		openWindow();
	}
}

function onError(item) {
	returnError("Can't get URL from Options");
}

function openURL() {
	var item=browser.storage.local.get("jira");
	item.then(onGot, onError);
}

function inputKeyListener(e) {
    var enter=13;

	if (e.keyCode === enter) {
		openURL();
	}
};

function listenInputKey(inputKey) {
	if (inputKey.addEventListener) {
		inputKey.addEventListener("keydown", inputKeyListener, false);
	} else if (inputKey.attachEvent) {
		inputKey.attachEvent("keydown", inputKeyListener);
	}
};

function listenKey() {
	listenInputKey(document.getElementById("key"));
};

if (window.addEventListener) {
	window.addEventListener("load", this.listenKey, false);
} else if (window.attachEvent) {
	window.attachEvent("onload", this.listenKey);
} else {
	document.addEventListener("load", this.listenKey, false);
}
