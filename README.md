## NB! This is a dead project! It's not supported anymore, so there is no guarantee that it will work on latest versions of browser.

# Short Description
Extension opens JIRA issue in new tab. User needs only to insert issue key.

It is useful for developers, analysts or testers who often need to open JIRA issue using its key.

[Same extension for Chrome](https://gitlab.com/irina-ivanova-extensions/JIRA-Issue-Opener "Same extension for Chrome")

# Download and Install
* **[Install](https://addons.mozilla.org/en-US/firefox/addon/jira-issue-opener/ "Install")** last version from Mozilla Store

# Questions and Comments
Any questions or comments are welcome! You can write me an e-mail on [irina.ivanova@protonmail.com](mailto:irina.ivanova@protonmail.com "irina.ivanova@protonmail.com") or an issue here.

# Description
Basically extension simply adds your issue key to specified URL and opens it in new tab:

`URL/browse/KEY`

Where `URL` is parameter, that user should specify in Options page (only one time after installation) and `KEY` is issue key that user inserts into extension field.

**Features**
* `KEY` is case insensitive
* All spaces in the beginning and in the end will be trimmed
* Skype formatting will be trimmed: `[16.09.2014 13:34:34] Irina Ivanova: KEY-776` will be recognized as `KEY-776`


# Posts About JIRA Issue Opener
* *January 5, 2015* [Getting Started With FireFox Extensions](http://ivanova-irina.blogspot.com/2015/02/getting-started-with-firefox-extensions.html "Getting Started With FireFox Extensions")
* *September 17, 2014* [JIRA Issue Opener v1.1](http://ivanova-irina.blogspot.com/2014/09/jira-issue-opener-v11.html "JIRA Issue Opener v1.1")
* *September 12, 2014* [JIRA Issue Opener v1.0](http://ivanova-irina.blogspot.com/2014/09/jira-issue-opener-v10.html "JIRA Issue Opener v1.0")